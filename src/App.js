import React from "react";
import "antd/dist/antd.css";
import "./index.css";
import {
  LaptopOutlined,
  NotificationOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import CarouselForm from "./components/carousel/carousel";
import { Col } from "antd/lib/grid";
import Card from "antd/lib/card";
import { Button } from "antd";
import Cards from "./components/carousel/card/card";
import { icons } from "antd/lib/image/PreviewGroup";
import { FacebookOutlined , PhoneOutlined } from '@ant-design/icons';
import ContentForm from "./components/content/content";
import FooterForm from "./components/footer/footer";
import Contact from "./components/contact/contact";

const { Header, Content } = Layout;
const items1 = [
  "HOME",
  "DỰ ÁN",
  "BĐS BÁN",
  "CHUYỂN NHƯỢNG",
  "CHO THUÊ",
  "LIÊN HỆ",
].map((key) => ({
  key,
  label: `${key}`,
}));
const items2 = [UserOutlined, LaptopOutlined, NotificationOutlined].map(
  (icon, index) => {
    const key = String(index + 1);
    return {
      key: `sub${key}`,
      icon: React.createElement(icon),
      label: `subnav ${key}`,
      children: new Array(4).fill(null).map((_, j) => {
        const subKey = index * 4 + j + 1;
        return {
          key: subKey,
          label: `option${subKey}`,
        };
      }),
    };
  }
);

const App = () => (
  <Layout>
    <div className="contact-facebook" onClick={() => {
      window.location.href = "https://www.facebook.com/cong.duy.583671";
    }}>
      <FacebookOutlined className="icon"/>
      <label>Liên hệ facebook</label>
    </div>
    <div className="contact-phone" onClick={() => {
      window.location.href = "https://www.facebook.com/cong.duy.583671";
    }}>
      <PhoneOutlined className="icon spin"/>
      <label style={{margin:'0 0 0 100px'}}>093.666.1875</label>
    </div>
    <Header className="header">
      <img className="logo" src="https://bds28.vnwordpress.net/wp-content/uploads/2020/07/logo-real-estate.png"></img>
      <Menu
        theme="light"
        mode="horizontal"
        defaultSelectedKeys={["1"]}
        items={items1}
      />
    </Header>
    <Layout>
      <Layout style={{}}>
        <Content
          className="site-layout-background"
          style={{
            margin: 0,
            minHeight: 280,
          }}
        >
          <CarouselForm></CarouselForm>
        </Content>
        <ContentForm className="m-2"></ContentForm>
        <Cards className="m-2"></Cards>
        <Contact></Contact>
        <FooterForm></FooterForm>
      </Layout>
    </Layout>
  </Layout>
);

export default App;
