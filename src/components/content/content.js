import React from "react";
import { Col } from "antd";
import './index.css';

export default function ContentForm() {
  return (
    <>
      <Col span={24} align="middle" style={{padding:'0 0 20px'}}>
        <h1 className="heading" >Giới thiệu</h1>
      </Col>
      <Col md={{ span: 16, offset: 4 }}>
        
        <h3>
          Sàn giao dịch bất động sản Đất Xanh ra đời đã hiện thực hóa chuỗi giá
          trị dịch vụ khách hàng và giải pháp bán hàng. Cùng với đội ngũ nhân
          viên chuyên nghiệp, tận tâm, giàu kinh nghiệm đã góp một phần quan
          trọng vào việc hoàn thành sứ mệnh của Công ty, nâng khả năng cung cấp
          sản phẩm, dịch vụ của Đất Xanh lên một tầm cao mới.
        </h3>
        <h3>
          Tất cả các giải pháp mà Đất Xanh cung cấp đều được phân tích một cách
          chuyên sâu, hướng đến phục vụ và giải quyết những vướng mắc một cách
          nhanh chóng và thỏa mãn tối đa nhu cầu của khách hàng. Chúng tôi không
          chỉ cung cấp sản phẩm bất động sản đơn thuần mà còn mang đến cho khách
          hàng sự an tâm, tin tưởng và tự hào khi sở hữu sản phẩm của Đất Xanh.
        </h3>
        <h2>Dịch vụ của Đất Xanh bao gồm:</h2>
        <h2>Môi giới và tư vấn bất động sản:</h2>
        <h3>
          Dịch vụ môi giới và tư vấn bất động sản của Đất Xanh luôn làm gia tăng
          cơ hội giao dịch. Luôn thấu hiểu nhu cầu, lựa chọn của khách hàng,
          chúng tôi có kế hoạch và giải pháp bán hàng hiệu quả cũng như tư vấn
          các giải pháp về thị trường và giá cho chủ đầu tư dự án và khách hàng.
        </h3>
        <h2>Quản lý bất động sản và cho thuê căn hộ:</h2>
        <h3>
          Sàn giao dịch bất động sản Đất Xanh có đội ngũ chuyên viên tư vấn tận
          tâm, giàu kinh nghiệm sẽ giúp quý khách quản lý và giao dịch bất động
          sản phù hợp với nhu cầu và làm gia tăng giá trị các bất động sản của
          khách hàng.
        </h3>
        <h3>
          Với đội ngũ nhân viên chuyên nghiệp, tận tâm, giàu kinh nghiệm sẽ đảm
          bảo mang đến cho khách hàng sự an tâm, hài lòng khi đến với Đất Xanh.
        </h3>
      </Col>
    </>
  );
}
