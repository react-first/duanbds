import React from 'react'
import { Col } from "antd/lib/grid";
import FormContact from './formcontact';

export default function Contact() {
  return (
    <Col md={{span:18 , offset:3 }} align="middle">
        <h1 className='heading'>Liên hệ</h1>
        <FormContact></FormContact>
    </Col>
  )
}
