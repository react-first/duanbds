import React, { useEffect, useState } from "react";
import { Col } from "antd/lib/grid";
import Card from "antd/lib/card";
import { Button, Modal, Image } from "antd";
import dataProject from "./data.json";
import "./index.css";
import ModalForm from "../modal/modal";
export default function Cards() {
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(dataProject);
  });

  const [dataClick, SetDataClick] = useState([]);
  useEffect(() => {
    onBtnClick(dataClick);
  }, []);

  const onBtnClick = (dataClick) => {
    SetDataClick(dataClick);
  };
  const [image,SetImage] = useState('')
  const [visible, setVisible] = useState(false);

  return (
    <>
      <Col md={{ span: 24 }} type="flex" align="middle" className="cards">
        <h1 className="heading">Dự án tiêu biểu</h1>
        <Col md={{ span: 16, offset: 3 }}>
          {data.map((data) => {
            return (
              <Card
                hoverable
                style={{
                  width: 350,
                  float: "left",
                  margin: 20,
                  flexWrap: "wrap",
                }}
                cover={
                  <>
                    <Image
                      preview={{
                        visible: false,
                      }}
                      width='100%'
                      height= '250px'
                      src={data.url}
                      onClick={() => {setVisible(true);SetImage(data.url)}}
                    />
                    <div
                      style={{
                        display: "none",
                      }}
                      
                    >
                      <Image.PreviewGroup
                        preview={{
                          visible,
                          onVisibleChange: (vis) => {setVisible(vis)},
                        }}
                      >
                        <Image src={image}/>
                      </Image.PreviewGroup>
                    </div>
                  </>
                }
              >
                <h1 className="card-heading">{data.name}</h1>
                <h1>Địa chỉ: {data.address}</h1>
                <h1>Diện tích: {data.acreage} m2</h1>
                <ModalForm data={data}></ModalForm>
              </Card>
            );
          })}
        </Col>
      </Col>
    </>
  );
}
