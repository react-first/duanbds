import React, { useEffect } from 'react';
import 'antd/dist/antd.css';
import './index.css';
import { Button, Modal , Input } from 'antd';
import { useState } from 'react';
import TextArea from 'antd/lib/input/TextArea';
import { height } from '@mui/system';

const ModalForm = (data) => {
  
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <>
      <Button type="primary" onClick={showModal}>
        Chi tiết
      </Button>
      <Modal title="Chi tiết dự án" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <h2>Tên dự án</h2>
        <Input value={data.data.name}></Input>
        <h2>Địa chỉ</h2>
        <Input value={data.data.address}></Input>
        <h2>Mô tả</h2>
        <TextArea style={{height:'200px'}} value={data.data.description}></TextArea>
        <h2>Diện tích</h2>
        <Input value={data.data.acreage}></Input>
        <h2>Số toà </h2>
        <Input value={data.data.numBlock}></Input>
        <h2>Số tầng</h2>
        <Input value={data.data.numFloors}></Input>
        <h2>Số căn hộ</h2>
        <Input value={data.data.numApartment}></Input>
      </Modal>
    </>
  );
};

export default ModalForm;