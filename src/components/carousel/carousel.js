import React from "react";
import "antd/dist/antd.css";
import "./index.css";
import { Carousel } from "antd";

const contentStyle1 = {
  height: "800px",
  color: "#fff",
  lineHeight: "250px",
  textAlign: "center",
  backgroundImage:
    'url("https://bds30.vnwordpress.net/wp-content/uploads/2018/03/bn1.jpg")',
    backgroundSize: 'cover',
};
const contentStyle2 = {
  height: "800px",
  color: "#fff",
  lineHeight: "250px",
  textAlign: "center",
  backgroundImage:
    'url("https://bds30.vnwordpress.net/wp-content/uploads/2018/03/bn2.jpg")',
  backgroundSize: 'cover',
};
const contentStyle3 = {
  height: "800px",
  color: "#fff",
  lineHeight: "250px",
  textAlign: "center",
  backgroundImage:
    'url("https://is.vnecdn.net/v014/21/28/18/4182821/assets/images/cover-pc.jpg")',
  backgroundSize: 'cover',
};

const CarouselForm = () => (
  <Carousel autoplay >
    <div>
      <h3 style={contentStyle1}></h3>
    </div>
    <div>
      <h3 style={contentStyle2}></h3>
    </div>
    <div>
      <h3 style={contentStyle3}></h3>
    </div>
  </Carousel>
);

export default CarouselForm;
